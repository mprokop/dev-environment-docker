FROM ubuntu:latest

ENV TERRAFORM_VERSION=1.1.2

RUN DEBIAN_FRONTEND="noninteractive" apt-get update && apt-get -y install tzdata
#RUN apt-get -y install ruby ruby-dev gcc g++ make iputils-ping wget git net-tools
RUN apt-get -y install gcc g++ make iputils-ping wget git net-tools unzip
#RUN apt-get install gem -y
RUN apt-get install vim -y
#RUN gem install json -v 1.8.5
#RUN gem install acirb
#RUN gem install inspec-bin
#RUN rm -rf /usr/lib/ruby/gems/2.7.0/specifications/default/json-2.3.0.gemspec
#RUN rm -R /usr/lib/ruby/gems/2.7.0/gems/json-2.3.0
#ADD ./config/plugins.json /root/.inspec/plugins.json

RUN apt-get install zsh -y
RUN wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh || true
ADD ./config/.zshrc /root/.zshrc

#RUN apt-get install golang gox curl gnupg2 software-properties-common lsb-release -y
RUN apt-get install curl gnupg2 software-properties-common lsb-release -y
RUN curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -
RUN apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
RUN apt-get update && apt-get install terraform

RUN apt-get install software-properties-common -y
RUN apt-add-repository --yes --update ppa:ansible/ansible
RUN apt-get install ansible -y

RUN apt-get install python3-pip -y

RUN git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
ADD ./config/.p10k.zsh /root/.p10k.zsh

RUN apt-get install sudo -y
#RUN curl https://raw.githubusercontent.com/terraform-linters/tflint/master/install_linux.sh | bash

RUN pip3 install anybadge git-python attrs Flask

#acitoolkit
RUN git clone https://github.com/datacenter/acitoolkit.git
RUN python3 ./acitoolkit/setup.py install

#AWS CLI
RUN apt-get install awscli -y
ADD ./config/credentials /root/.aws

#Install golang
RUN wget https://go.dev/dl/go1.17.5.linux-amd64.tar.gz
RUN rm -rf /usr/local/go && tar -C /usr/local -xzf go1.17.5.linux-amd64.tar.gz
ENV PATH=$PATH:/usr/local/go/bin

CMD [ "zsh" ]

WORKDIR /home/git
